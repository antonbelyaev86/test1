<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ResponseSubscriber implements EventSubscriberInterface
{
    /**
     * Get subscribed events.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => 'onKernelView',
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    /**
     * On kernel view.
     *
     * @param ViewEvent $event
     *
     * @throws
     */
    public function onKernelView(ViewEvent $event)
    {
        $request = $event->getRequest();
        $result = $event->getControllerResult();

        $event->setResponse($this->getSuccessResponse($request, $result));
    }

    /**
     * On kernel exception.
     *
     * @param ExceptionEvent $event
     *
     * @throws
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $request = $event->getRequest();

        $params = [
            'errorCode' => 104,
            'errorDesc' => 'Внутренняя ошибка',
            'errorName' => 'Internal error',
        ];

        $response = $this->getErrorResponse($request, $params);
        $response->setStatusCode(200);

        $event->allowCustomResponseCode();
        $event->setResponse($response);
    }

    /**
     * Get error response.
     *
     * @param Request $request
     * @param         $params
     *
     * @return JsonResponse
     */
    protected function getErrorResponse($request, $params)
    {
        return $this->getResponse(['status' => 'error', 'error' => $params]);
    }

    /**
     * Get success response.
     *
     * @param Request $request
     * @param         $params
     *
     * @return JsonResponse
     */
    protected function getSuccessResponse($request, $params)
    {
        if (is_bool($params)) {
            return $this->getResponse();
        }

        return $this->getResponse(['status' => 'ok', 'response' => $params]);
    }

    /**
     * Get response.
     *
     * @param mixed   $data
     *
     * @return JsonResponse|Response
     */
    protected function getResponse($data = null)
    {
        if (is_null($data)) {
            $response = new JsonResponse();
        } else {
            $response = new JsonResponse($data);
            $headers['Content-type'] = 'application/json; charset=utf-8';
        }
        $response->headers->add($headers);

        return $response;
    }
}