# Задание

С использованием php, mongoDB и фреймворка symfony необходимо реализовать метод restful API

GET /api/v1/posts - Получить список постов

# Ответ сервера
Серверное приложение возвращает данные в формате JSON. Стандартная структура ответа серверного приложения выглядит 
следующим образом:

`
{
    “status”: “ok”,
    “response”: {}
}
`

# Ошибки
В случае неудачного выполнения запроса сервер возвращает ответ с данными произошедшей ошибки, пример

`
{
    "status": "error",
    "error": {
        "errorCode": 1,
        "errorDesc": "Внутренняя ошибка",
        "errorName": "Internal error"
    }
}
`

## Установка

Перед использованием необходимо выполнить 
```bash
composer install
```
дамп БД можно скачать по ссылке ссылке
[Дамп](https://drive.google.com/file/d/1T7cBL8Sdnngj-qyDCLqLG_H62w59mT5F)

Восстановить командой
```bash
mongorestore -v --gzip --objcheck --archive=tbdb_dump.gz --db=<your_database> --username=<username> --password=<password>
```
