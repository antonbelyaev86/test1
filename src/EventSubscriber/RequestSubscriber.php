<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestSubscriber implements EventSubscriberInterface
{
    //можно переделать на конфиг
    private $versions = [
        'v1',
        'v2'
    ];

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 1000],
        ];
    }

    /**
     * On kernel request.
     *
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $parts = explode("/", $event->getRequest()->getPathInfo());
        if (count($parts) < 3 || !in_array($parts[2], $this->versions)) {
            return;
        }

        $event->getRequest()->attributes->set('version', $parts[2]);
    }
}