<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Service\PostManager;

/**
 * Class PostController
 *
 * @Rest\Version("v1")
 */
class PostController extends AbstractController
{
    /**
     * @param Request $request
     * @Rest\Route("/posts", methods="GET", name="post_list")
     * @return array
     */
    public function listAction(Request $request, PostManager $postManager)
    {
        $posts = $postManager->getPosts();

        return $posts;
    }
}