<?php

namespace App\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use App\Document\Post;

class PostManager
{
    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @param DocumentManager $dm
     *
     * @required
     */
    public function setDI(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function getPosts()
    {
        $posts = [];
        $postsData = $this->dm->getRepository(Post::class)->findAll();

        /** @var Post $post */
        foreach ($postsData as $post) {
            $posts[] = $post->toView();
        }

        return $posts;
    }
}