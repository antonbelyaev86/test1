<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB; //(repositoryClass="Repository\PostRepository")

/**
 * @MongoDB\Document(
 *     collection="posts"
 * )
 */
class Post
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $citySlug;

    /**
     * @MongoDB\Field(type="int")
     */
    private $commentCount;

    /**
     * @MongoDB\Field(type="collection")
     */
    private $comments;

    /**
     * @MongoDB\Field(type="timestamp")
     */
    private $createdAt;

    /**
     * @MongoDB\Field(type="boolean")
     */
    private $hasOutletLike;

    /**
     * @MongoDB\Field(type="collection")
     */
    private $imageSets;

    /**
     * @MongoDB\Field(type="boolean")
     */
    private $isEdited;

    /**
     * @MongoDB\Field(type="int")
     */
    private $likeCount;

    /**
     * @MongoDB\Field(type="collection")
     */
    private $likes;

    /**
     * @MongoDB\Field(type="string")
     */
    private $placeId;

    /**
     * @MongoDB\Field(type="string")
     */
    private $placeSlug;

    /**
     * @MongoDB\Field(type="string")
     */
    private $rating;

    /**
     * @MongoDB\Field(type="string")
     */
    private $status;

    /**
     * @MongoDB\Field(type="string")
     */
    private $text;

    /**
     * @MongoDB\Field(type="string")
     */
    private $type;

    /**
     * @MongoDB\Field(type="string")
     */
    private $userId;

    /**
     * @MongoDB\Field(type="string")
     */
    private $userSlug;

    /**
     * @MongoDB\Field(type="collection")
     */
    private $galleries;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCitySlug()
    {
        return $this->citySlug;
    }

    /**
     * @param mixed $citySlug
     */
    public function setCitySlug($citySlug): void
    {
        $this->citySlug = $citySlug;
    }

    /**
     * @return mixed
     */
    public function getCommentCount()
    {
        return $this->commentCount;
    }

    /**
     * @param mixed $commentCount
     */
    public function setCommentCount($commentCount): void
    {
        $this->commentCount = $commentCount;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments): void
    {
        $this->comments = $comments;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * @param mixed $galleries
     */
    public function setGalleries($galleries): void
    {
        $this->galleries = $galleries;
    }

    /**
     * @return mixed
     */
    public function getHasOutletLike()
    {
        return $this->hasOutletLike;
    }

    /**
     * @param mixed $hasOutletLike
     */
    public function setHasOutletLike($hasOutletLike): void
    {
        $this->hasOutletLike = $hasOutletLike;
    }

    /**
     * @return mixed
     */
    public function getImageSets()
    {
        return $this->imageSets;
    }

    /**
     * @param mixed $imageSets
     */
    public function setImageSets($imageSets): void
    {
        $this->imageSets = $imageSets;
    }

    /**
     * @return mixed
     */
    public function getisEdited()
    {
        return $this->isEdited;
    }

    /**
     * @param mixed $isEdited
     */
    public function setIsEdited($isEdited): void
    {
        $this->isEdited = $isEdited;
    }

    /**
     * @return mixed
     */
    public function getLikeCount()
    {
        return $this->likeCount;
    }

    /**
     * @param mixed $likeCount
     */
    public function setLikeCount($likeCount): void
    {
        $this->likeCount = $likeCount;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $likes
     */
    public function setLikes($likes): void
    {
        $this->likes = $likes;
    }

    /**
     * @return mixed
     */
    public function getPlaceId()
    {
        return $this->placeId;
    }

    /**
     * @param mixed $placeId
     */
    public function setPlaceId($placeId): void
    {
        $this->placeId = $placeId;
    }

    /**
     * @return mixed
     */
    public function getPlaceSlug()
    {
        return $this->placeSlug;
    }

    /**
     * @param mixed $placeSlug
     */
    public function setPlaceSlug($placeSlug): void
    {
        $this->placeSlug = $placeSlug;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserSlug()
    {
        return $this->userSlug;
    }

    /**
     * @param mixed $userSlug
     */
    public function setUserSlug($userSlug): void
    {
        $this->userSlug = $userSlug;
    }

    public function toView()
    {
        return [
            'id' => $this->getId(),
            'citySlug' => $this->getCitySlug(),
            'commentCount' => $this->getCommentCount(),
            'comments' => $this->getComments(),
            'createdAt' => $this->getCreatedAt(),
            'hasOutletLike' => $this->getHasOutletLike(),
            'imageSets' => $this->getImageSets(),
            'isEdited' => $this->getisEdited(),
            'likeCount' => $this->getLikeCount(),
            'likes' => $this->getLikes(),
            'placeId' => $this->getPlaceId(),
            'placeSlug' => $this->getPlaceSlug(),
            'rating' => $this->getRating(),
            'status' => $this->getStatus(),
            'text' => $this->getText(),
            'type' => $this->getType(),
            'userId' => $this->getUserId(),
            'userSlug' => $this->getUserSlug(),
            'galleries' => $this->getGalleries(),
        ];
    }
}